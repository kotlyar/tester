import { Mongo } from "meteor/mongo";

export const Testers = new Mongo.Collection("testers");

export const Info = new Mongo.Collection("testerinfo");

export const Results = new Mongo.Collection("results");
