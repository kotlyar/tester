import React, { Component } from "react";

import InfoForm from "../components/createTester/InfoForm";

export default class CreatePage extends Component {
  render() {
    return (
      <div>
        <h3>Создание теста</h3>
        <InfoForm />
      </div>
    );
  }
}
