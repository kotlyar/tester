import React from "react";

import { Router, Route, Switch } from "react-router";
import createBrowserHistory from "history/createBrowserHistory";

import { Navbar, Container, Nav } from "react-bootstrap";

import Hello from "./Hello";
import CreatePage from "./pages/Create";
// route components
// import AppContainer from '../../ui/containers/AppContainer.js';
// import ListPageContainer from '../../ui/containers/ListPageContainer.js';
// import AuthPageSignIn from '../../ui/pages/AuthPageSignIn.js';
// import AuthPageJoin from '../../ui/pages/AuthPageJoin.js';
// import NotFoundPage from '../../ui/pages/NotFoundPage.js';

const browserHistory = createBrowserHistory();

const App = () => (
  <Container>
    <Navbar expand="lg" variant="light" bg="light">
      <Navbar.Brand href="/">Тестер</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/create">Создать тест</Nav.Link>
          <Nav.Link href="/tester">Пройти тест</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>

    <Router history={browserHistory}>
      <Switch>
        <Route exact path="/" component={Hello} />
        <Route exact path="/create" component={CreatePage} />
      </Switch>
    </Router>
  </Container>
);

export default App;
