import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";

export default class InfoForm extends Component {
  state = {
    title: "Новый тест",
    skill: 1,
    lang: "ru"
  };
  handlerSubmit = event => {
    event.preventDefault();
    console.log(this.state);
  };
  changeField = field => event => {
    this.setState({ [field]: event.target.value });
  };

  render() {
    const { title, skill, lang } = this.state;
    return (
      <Form onSubmit={this.handlerSubmit}>
        <Form.Group controlId="formInfoTitle">
          <Form.Label>Название</Form.Label>
          <Form.Control
            defaultValue={title}
            type="text"
            placeholder="Название теста"
            onChange={this.changeField("title")}
          />
        </Form.Group>

        <Form.Group controlId="formInfoSkill">
          <Form.Label>Направление</Form.Label>
          <Form.Control
            as="select"
            onChange={this.changeField("skill")}
            defaultValue={skill}
          >
            <option>Scratch</option>
            <option>Python</option>
            <option>Pygame</option>
          </Form.Control>
        </Form.Group>
        <Form.Group controlId="formInfoLang">
          <Form.Label>Язык</Form.Label>
          <Form.Control
            as="select"
            onChange={this.changeField("lang")}
            defaultValue={lang}
          >
            <option>ru</option>
            <option>en</option>
          </Form.Control>
        </Form.Group>
        <Button variant="primary" type="submit">
          Создать
        </Button>
      </Form>
    );
  }
}
